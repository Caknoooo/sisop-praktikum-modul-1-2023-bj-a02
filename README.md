# Praktikum 1 Sistem Operasi 
Perkenalkan kami dari kelas ``Sistem Operasi A Kelompok  A02``, dengan Anggota sebagai berikut:

| Nama                      | NRP        |
|---------------------------|------------|
|Wan Sabrina Mayzura        | 5025211023 |
|M. Armand Giovani          | 5025211054 |
|M. Naufal Badruttamam      | 5025211240 |

Berikut merupakan demo untuk praktikum 1

## 1️⃣ Nomor 1

Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 

### Point A

Soal

Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

Input:
```sh
awk -F"," '/Japan/ {print $2, $9}' '2023 QS World University Rankings.csv' | head -5
```

Output:
```
Menampilkan 5 universitas tertinggi di negara jepang
Tokyo Institute of Technology (Tokyo Tech) 81.5
Tohoku University 98.6
The University of Tokyo 91.9
Osaka University 67.4
Kyoto University 94.8
```

![image](https://user-images.githubusercontent.com/92671053/224318763-090d2c05-f39a-47b3-a504-c7b8b66c1ec7.png)

Penjelasan 
- ``awk`` sendiri merupakan command yang dimiliki oleh Linux, awk biasanya digunakan sebagai alat untuk memanipulasi teks pada sistem operasi Unix dan sebagainya
- ``-F","`` merupakan sebuah command yang dimiliki oleh ``awk`` yang digunakan sebagai separator atau pemisah antara kolom satu dengan kolom yang lain. Kasus disini "," merupakan pemisah antar kolom
- ``'/Japan/'`` merupakan sebuah pola yang digunakan untuk mencari baris yang mengandung kata ``Japan`` pada suatu file tertentu
- ``{print $2 $9}`` merupakan perintah untuk mencetak isi kolom kedua dan kolom kesembilan dari suatu baris yang sebelumnya telah di filter
- ``2023 QS World University Rankings.csv`` merupakan nama file yang akan diekskusi oleh ``awk``
- `head -5` merupakan suatu command untuk mencetak lima baris pertama sesuai syarat-syarat yang telah diberikan

### Point B
Soal 
Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a.

Input:
```sh
awk '/Japan/' "2023 QS World University Rankings.csv" | sort -t',' -k9,9n | sed -n 1,5p | cut -d',' -f2
```

Output:
```
Menampilkan nilai terendah dari 5 universitas di jepang
Ritsumeikan Asia Pacific University
Shibaura Institute of Technology
Kwansei Gakuin University
Doshisha University
Meiji University
```
![image](https://user-images.githubusercontent.com/92671053/224318852-775cb489-f2ce-4b39-99f8-b03eb36222d9.png)

Penjelasan 
- ``awk`` sendiri merupakan command yang dimiliki oleh Linux, awk biasanya digunakan sebagai alat untuk memanipulasi teks pada sistem operasi Unix dan sebagainya
- ``'/Japan/' "2023 QS World University Rankings.csv"`` merupakan command yang digunakan untuk mencari baris yang mengandung kata "Japan" pada file "2023 QS World University Rankings.csv"
- ``sed -n 1,5p`` sed sendiri merupakan singkatan dari ``stream editor`` disini menandakan bahwa dia bisa customisasi terkait hal yang ingin dikeluarkan oleh perintah awk sebelumnya. Jadi, sed disini untuk mengeluarkan baris 1 hingga 5 dari output yang dihasilkan
- ``sort -t, -k9,9n`` merupakan command yang digunakan untuk mengurutkan data secara numerik berdasarkan kolom ke-9, urutannya dari belakang (dari nilai yang paling besar ke nilai yang paling kecil). Lalu ada opsi ``-t,`` menandakan bahwa separator atau pemisah antar kolom tersebut adalah koma
- ``cut -d',' -f2`` merupakan command yang digunakan untuk memotong output dari kolom kedua ``(-f2)`` dengan menggunakan pemisah koma karena csv yang diberikan menggunakan ``,`` sebagai separator nya

### Point C
Soal
Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.

Input:
```sh
awk '/Japan/' "2023 QS World University Rankings.csv" | sed -n 1,10p | sort -t',' -k20 -nr | cut -d',' -f2
```

Output:
```
Menampilkan 10 universitas ger rank tertinggi
The University of Tokyo
Keio University
Waseda University
Hitotsubashi University
Tokyo University of Science
Kyoto University
Nagoya University
Tokyo Institute of Technology (Tokyo Tech)
International Christian University
Kyushu University
```
![image](https://user-images.githubusercontent.com/92671053/224319089-cb210045-89d9-40cb-a201-49503defff18.png)

Penjelasan
- ``awk`` sendiri merupakan command yang dimiliki oleh Linux, awk biasanya digunakan sebagai alat untuk memanipulasi teks pada sistem operasi Unix dan sebagainya
- ``'/Japan/' "2023 QS World University Rankings.csv"`` merupakan command yang digunakan untuk mencari baris yang mengandung kata "Japan" pada file "2023 QS World University Rankings.csv"
- ``sed -n 1,10p`` sed sendiri merupakan singkatan dari ``stream editor`` disini menandakan bahwa dia bisa customisasi terkait hal yang ingin dikeluarkan oleh perintah awk sebelumnya. Jadi, sed disini untuk mengeluarkan baris 1 hingga 510 dari output yang dihasilkan
- ``sort -t, -k20 -nr`` merupakan command yang digunakan untuk mengurutkan data secara numerik berdasarkan kolom ke-20, urutannya dari belakang (dari nilai yang paling besar ke nilai yang paling kecil). Lalu ada opsi ``-t,`` menandakan bahwa separator atau pemisah antar kolom tersebut adalah koma
- ``cut -d',' -f2`` merupakan command yang digunakan untuk memotong output dari kolom kedua ``(-f2)`` dengan menggunakan pemisah koma karena csv yang diberikan menggunakan ``,`` sebagai separator nya

### Point D
Soal 
Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

Input:
```sh
awk '/Keren/' "2023 QS World University Rankings.csv" | cut -d',' -f2
```

Output:
```
Menampilkan Universitas paling keren :)
Institut Teknologi Sepuluh Nopember (ITS Surabaya Keren)
```
![image](https://user-images.githubusercontent.com/92671053/224319157-61a08db4-7350-4fd8-8a60-7137d4a148d8.png)

Penjelasan
- ``awk`` sendiri merupakan command yang dimiliki oleh Linux, awk biasanya digunakan sebagai alat untuk memanipulasi teks pada sistem operasi Unix dan sebagainya
- ``'/Keren/' "2023 QS World University Rankings.csv"`` merupakan command yang digunakan untuk mencari baris yang mengandung kata "Keren" pada file "2023 QS World University Rankings.csv"
- ``cut -d',' -f2`` merupakan command yang digunakan untuk memotong output dari kolom kedua ``(-f2)`` dengan menggunakan pemisah koma karena csv yang diberikan menggunakan ``,`` sebagai separator nya

### Hasil Keseluruhan 
![image](https://user-images.githubusercontent.com/92671053/224319612-c22af2ff-95bc-4cd3-a9fa-0aac40e4daa6.png)

## 2️⃣ Nomor 2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

### Problem 2a
Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat(kumpulan_1, kumpulan_2, dst) 

### Solusi
- Pertama-tama deklarasi fungsi 2a dengan sintaks `2a()`

- Kemudian lakukan looping untuk cek apakah folder `kumpulan_$loop` sudah ada atau belum di dalam directori. Jika folder kumpulan tidak ada maka looping akan berhenti dan akan mendapatkan nilai `i`.

- Setelah itu buat command untuk membuat folder baru 
```sh 
mkdir kumpulan_$i
``` 
dengan nilai `i` yang didapatkan dari looping sebelumnya

- Setelah folder dibuat masuk ke dalam folder tersebut menggunakan command 
```sh 
cd kumpulan_$i
``` 

- Kemudian, buat fungsi untuk mendownload gambar dengan menggunakan loop dengan nilai awal 1 sampai waktu sekarang `$(date +"%H")` untuk mendownload gambar dengan total gambar sesuai dengan waktu sekarang dengan ketentuan tambahan jika waktu sekarang 00 maka hanya akan mendownload 1 gambar saja.
```sh
# Looping sesuai waktu
if [ "$(date +"%H")" == "00" ]; then
  # Download satu gambar
  wget https://source.unsplash.com/random/800x600/?indonesia -O perjalanan_1.jpg
else
  for ((j=1; j<=$(date +"%H"); j++))
  do
    # Download gambar
    wget https://source.unsplash.com/random/800x600/?indonesia -O perjalanan_$j.jpg
  done
fi

```
Dari fungsi diatas akan mendownload gambar yang bernama `perjalanan_$j` dengan `$j` sesuai urutan download gambar.

#### CronJob
Permintaan Soal yaitu program dapat mendownload gambar secara otomatis setiap 10 jam setelah program pertama dijalankan, maka perintah cronjob yang benar adalah
```
0 */10 * * * /bin/bash /home/vangarman/Documents/Praktikum\ Sisop/sisop-praktikum-modul-1-2023-bj-a02/soal2/kobeni_liburan.sh 2a
```
Detail :
- `0` menunjukkan bahwa fungsi 2a akan dijalankan pada menit ke-0 setiap jam.
- `*/10` menunjukkan bahwa fungsi 2a akan dijalankan setiap 10 jam.
- `*/10 * * *` menunjukkan bahwa fungsi 2a akan dijalankan setiap 10 jam pada setiap hari.
- `/bin/bash` digunakan untuk menunjukkan bahwa perintah dijalankan dengan menggunakan shell bash.
- `/home/vangarman/Documents/Praktikum\ Sisop/sisop-praktikum-modul-1-2023-bj-a02/soal2/kobeni_liburan.sh` menunjukkan path dari file `kobeni_liburan.sh`
- `2a` menunjukkan fungsi yang dijalankan dalam file kobeni_liburan.sh.


## Dokumentasi dan Kendala
- Pengerjaan kobeni_liburan.sh
![pengerjaan kobeni sh](https://user-images.githubusercontent.com/100523471/224270373-9c41ed3d-c64e-4c49-857f-74a5a4626524.png)

- Pemanggilan fungsi 2a
![2a](https://user-images.githubusercontent.com/100523471/224270386-78988b84-58ec-4b4a-bb1d-952b68bf573a.png)

- Hasil pemanggilan Fungsi 2a
![hasil 2a](https://user-images.githubusercontent.com/100523471/224270401-e6aa1090-0ae6-4ee1-affc-7e6541370274.png)

Kendala yang dialami ketika Pengerjaan Problem 2a yaitu ketika dijalankan program 2a tidak membuat folder baru, karena asih kurang kondisi percabangan untuk mengecek apakah directori tempat download itu sudah ada atau belum.

### Problem 2b
Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

### Solusi
- Pertama-tama deklarasi dulu fungsi 2b dengan sintaks `2b()`

- Kemudian, lakukan loop dengan variabel `k` dimulai dari 1 untuk memeriksa apakah folder `kumpulan_$k` sudah ada, jika folder belum ada maka loop akan berhenti. Kemudian dalam loop juga memeriksa apakah file `devil_$k.zip` sudah ada dalam direktori kerja saat ini, jika file sudah ada, loop akan dilanjutkan dengan iterasi selanjutnya. 
```sh
for ((k=1; ;k++)) do
if [ ! -d kumpulan_$k ]
  then
     break
  fi
  
  if [ -f devil_$k.zip ]
  then
    continue
  fi
```

- Jika file zip belum ada, fungsi akan melakukan zip dengan perintah `zip`dengan opsi `-r` untuk mengompres folder `kumpulan_$k` menjadi file `devil_$k.zip`
```sh
# Zip folder kumpulan
    zip -r devil_$k.zip kumpulan_$k
 
```
- Loop akan terus berjalan sampai tidak ada lagi folder `kumpulan_$k` ditemukan didalam direktori

- Setelah loop selesai, fungsi akan selesai di eksekusi

#### CronJob
Permintaan Soal yaitu program dapat melakukan zip setiap 1 hari secara otomatis, maka perintah cronjob yang benar adalah
```
@daily /bin/bash /home/vangarman/Documents/Praktikum\ Sisop/sisop-praktikum-modul-1-2023-bj-a02/soal2/kobeni_liburan.sh 2b
```
Detail :
- `@daily` menunjukkan bahwa perintah kedua akan dijalankan setiap hari, yaitu pada pukul 00:00 setiap harinya.
- `/bin/bash` digunakan untuk menunjukkan bahwa perintah dijalankan dengan menggunakan shell bash.
- `/home/vangarman/Documents/Praktikum\ Sisop/sisop-praktikum-modul-1-2023-bj-a02/soal2/kobeni_liburan.sh` menunjukkan path dari file `kobeni_liburan.sh`
- `2b` sebagai fungsi yang dijalankan dalam file kobeni_liburan.sh.

## Dokumentasi dan Kendala
- Pemanggilan Fungsi 2b
![2b](https://user-images.githubusercontent.com/100523471/224270397-e0e79187-56d0-4521-82e6-426ad4b2e097.png)

- Hasil Pemanggilan Fungsi 2b
![hasil 2b](https://user-images.githubusercontent.com/100523471/224270409-9f82e3f9-2062-40ce-8480-322011981126.png)

Adapun kendala untuk persoalan 2b yaitu ketika ingin dijalankan fungsi 2b, program yanng dijalankan itu program 2a, hal itu karena belum ditentukan bagaimana argumen pemanggilan fungsi 2a dan 2b.

## 3️⃣ Nomor 3
Membuat sistem Registrasi dan Login dengan menyimpan sistem register pada script ``louis.sh`` dari setiap user yang berhasil didaftarkan ke dalam file /users/user.txt, lalu sistem login dibuat pada script ``retep.sh``. 

### Langkah-langkah 
- Membuat file log.txt, jalankan command berikut
```
touch log.txt 
```
- Membuat folder users untuk menyimpan file ``user.txt`` yang akan berisikan informasi terkait dengan username dan password
```
mkdir users
cd users
touch user.txt
```
### louis.sh
- Untuk file `louis.sh` berisi tanggal dengan format ``YY/MM/DD hh:mm:ss `` yang akan disimpan pada file ``log.txt`` 
```
tanggal=$(date '+%m/%d/%Y %H:%M:%S')
``` 
- Selain itu, ada dua input yang dibutuhkan yaitu username dan password
- Untuk password, menggunakan ``read -s`` dengan tujuan agar password lebih aman, -s  disini berarti seek yang artinya menyembunyikan
- Pada file ``louis.sh`` terdapat function ``check_user`` sebagai berikut 
```sh
function check_user() {
  if grep -qi ${username} ./users/user.txt; then
    echo "Username sudah terdaftar, silahkan masukkan username yang lain"
    echo "${tanggal} REGISTER:ERROR user already exist" >> log.txt
  else
    return 0
  fi
  return 1
}
```
- Function ``check_user`` bertujuan untuk melakukan pengecekan apakah username yang dibuat sudah terdaftar atau belum, juka sudah maka akan keluar ``"Username sudah terdaftar, silahkan masukkan username yang lain"`` dan akan mengirimkan pesan `` REGISTER:ERROR user already exist``ke dalam file log.txt. Lalu pada soal, username tidak diperbolehkan mengandung kata ``chicken | ernie``
- Lalu ada function ``check_password`` sebagai berikut:
```sh
function check_password() {
  if [[ ${#password} -lt 8 ]]; then
    echo "Password harus memiliki minimal 8 karakter!"
  elif echo ${password} | grep -Eq "chicken|ernie"; then
    echo "Password tidak boleh mengandung 'chicken' atau 'ernie'"
  else
    if [[ ! ${password} =~ [A-Z] ]]; then
      echo "Password harus mengandung 1 huruf kapital"
    elif [[ ! ${password} =~ [a-z] ]]; then
      echo "Password harus mengandung 1 huruf kecil"
    elif [[ ! ${password} =~ [0-9] ]]; then
      echo "Password harus mengandung 1 alphanumeric"
    elif [[ ! ${password} =~ [[:upper:]] || ! ${password} =~ [[:lower:]] ]]; then
      echo "Password harus mengandung minimal 1 huruf kapital dan 1 huruf kecil"
    elif [[ ${username} == ${password} ]]; then
      echo "Nama dan password tidak boleh sama"
    else
      echo "Registrasi berhasil 😀😀😀"
      echo "selamat datang $username"
      echo -e "Username : ${username} - Password : ${password}" >>./users/user.txt
      echo -e "${tanggal} Register:INFO User ${username} registered successfully" >>log.txt
    fi
  fi
}
```
- Pada function tersebut, dilakukan pengecekan terhadap password yang telah kita buat, untuk ``${#password} -lt 8`` melakukan pengecekan terhadap panjang password yang minimal 8 huruf
- ``! ${password} =~ [A-Z]`` digunakan untuk melakukan pengecekan apakah password mengandung 1 huruf kapital atau tidak, jika tidak maka akan mengeluarkan ``"Password harus mengandung 1 huruf kapital"``
- ``! ${password} =~ [a-z]`` digunakan untuk melakukan pengecekan apakah password mengandung 1 huruf kecil atau tidak, jika tidak maka akan mengeluarkan ``"Password harus mengandung 1 huruf kecil"``
- ``! ${password} =~ [[:upper:]] || ! ${password} =~ [[:lower:]]`` digunakan untuk melakukan pengecekan apakah semua huruf besar semua atau kecil semua
- ``${username} == ${password}`` digunakan untuk melakukan pengecekan apakah username dan password sama, jika sama maka password tersebut tidak aman sehingga akan melakukan response ``"Nama dan password tidak boleh sama"``

#### Jika Berhasil
![image](https://user-images.githubusercontent.com/92671053/224321171-7753d291-5e1c-484a-a806-5ed41657302f.png)

#### Jika Gagal
- Password < 8 digit

![image](https://user-images.githubusercontent.com/92671053/224321852-c235489c-9c45-4daf-a325-7ca4154544be.png)

- Username dan Password sama
![image](https://user-images.githubusercontent.com/92671053/224322369-4dfee14c-2845-46df-b124-8c173370e0ff.png)

- Password mengandung ``ernie|chicken``
![image](https://user-images.githubusercontent.com/92671053/224322726-b61414e5-358d-472b-91e5-1fe0046841d1.png)

#### user.txt 
![image](https://user-images.githubusercontent.com/92671053/224323026-9ce0114b-d66f-4251-8bf6-718372f49260.png)

#### log.txt
![image](https://user-images.githubusercontent.com/92671053/224323326-5c21772a-6c63-449b-9117-6b6eda5033a3.png)


### retep.sh
- Pada file ``retep.sh`` akan diberikan 2 pilihan sesuai dengan script berikut 
```sh
function main() {
  echo -e "Apakah anda ingin Login(1) atau Register(2)?"
  read pick

  if [ ${pick} -eq 1 ]; then
    login
  elif [ ${pick} -eq 2 ]; then
    register 
  fi
}
```
- Jika `user` menekan 1 maka akan masuk function ``login()`` dan jika `user` menekan 2 maka akan masuk function ``register()`` yang akan mengarah langsung ke file `louis.sh`
- Pada function ``login()`` terdapat script berikut
```sh
function login(){
  echo -e "\n----------------------------------------------------------------------"
  echo -e "Selamat Datang di Aplikasi Kami, silahkan login dengan Username anda!"
  echo -e "Enter Username: \c"
  read username

  echo -e "Enter Password: \c"
  read -s password
  echo -e "\n----------------------------------------------------------------------"

  tanggal=$(date +'%m/%d/%Y %H:%M:%S')
  tanggal_folder=$(date +'%Y-%m-%d')_$username

  if grep -qi $username ./users/user.txt && grep -qi $password ./users/user.txt; then
    loginSuccess
  else
    loginFailed
  fi
}
```
- Pada fungsi tersebut, kita akan memasukkan username dan password kita, selain itu kita juga akan menentukan tanggal dibuatnya. 
- Setelah itu akan dilakukan pengecekan, apakah username yang dibuat sudah terdaftar sebelumnya dengan melakukan script berikut ``grep -qi $username ./users/user.txt && grep -qi $password ./users/user.txt``, jika berhasil akan masuk ke dalam function ``loginSuccess()``, sedangkan jika gagal akan masuk function ``loginFailed()``. 
- Jika berhasil login, maka akan memanggil function ``loginSuccess()`` sebagai berikut
```sh
function loginSuccess(){
  echo -e "\nWelcome $username!!"
  echo -e "$tanggal LOGIN:INFO User $username logged in" >> log.txt
}
```
- ``"$tanggal LOGIN:INFO User $username logged in"`` akan dimasukkan ke dalam file `log.txt`
- Jika gagal login, maka akan memanggil function ``loginFailed()`` sebagai berikut
```sh
function loginFailed(){
  echo -e "$tanggal Login:ERROR failed login attemp on user $username" >> log.txt
  echo -e "Apakah anda sudah memiliki akun? Sudah(1) atau Belum(2)"
  read pick
  if [[ $pick -eq 1 ]]; then
    login
  elif [[ $pick -eq 2 ]]; then 
    register
  fi
}
```
- User akan dihadapkan dengan 2 pilihan, jika gagal login karena salah password maka user bisa mengetik (1) sedangkan jika user merasa belum membuat akun maka dapat mengetik (2)
- (1) -> Login Ulang
- (2) -> Registrasi, akan mengarah langsung kedalam file ``louis.sh``
 
#### Berhasil Login
![image](https://user-images.githubusercontent.com/92671053/224323699-a372fb45-5437-4f7b-8806-9df416c2951c.png)

#### Gagal Login (Akan diarahkan ke menu awal)
![image](https://user-images.githubusercontent.com/92671053/224324096-65ea26eb-28c7-442d-9e08-0963c3cbca54.png)

#### log.txt

![image](https://user-images.githubusercontent.com/92671053/224324285-ede25a78-896c-44f4-85e8-abe447ae94d1.png)

## 4️⃣ Nomor 4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan :
* Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
* Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
    * Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
    * Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
    * Setelah huruf z akan kembali ke huruf a
* Buat juga script untuk dekripsinya.
* Backup file syslog setiap 2 jam untuk dikumpulkan

#### Solusi
Untuk melakukan backup file, perlu membuat direktori untuk menyimpan file backup. Misalkan saya menyimpan file log_encrypt.sh dan log_decrypt.sh di ```/home/username/soal4```, untuk file backupnya saya simpan di ```/home/username/soal4/encrypted/``` dan ```/home/username/soal4/decrypted/```, dan misalkan nama username yang digunakan pada soal ini adalah sabrina.

Pertama-tama akan dibuat 2 file log_encrypt.sh dan log_decrypt

**Untuk membuat file log_encrypt.sh**
```
$ nano log_encrypt.sh
```
Source code:

```sh
#!/bin/bash

current_hour=$(date "+%H")
current_date=$(date "+%H:%M %d:%m:%Y")

backup_filename="/home/sabrina/soal4/encrypted/${current_date}.txt"

lowercase_alphabet=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')
uppercase_alphabet=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')

cat /var/log/syslog | tr "${lowercase_alphabet:0:26}${uppercase_alphabet:0:26}" "${lowercase_alphabet:${current_hour}:26}${uppercase_alphabet:${current_hour}:26}" > "${backup_filename}"
```
Penjelasan
* ```date "+%H"``` digunakan untuk mengembalikan jam saat ini dalam format 24-jam. 
* ```date "+%H:%M %d:%m:%Y"``` digunakan untuk mengembalikan tanggal dan waktu saat ini dalam format yang ditentukan.
* Pada baris kelima, membuat nama file backup dengan format HH:MM dd:mm:YYYY.txt.
* Pada baris ketujuh dan kedelapan, menginisialisasi string alfabet dalam huruf kecil dan besar yang diulang dua kali untuk memastikan enkripsi bisa dilakukan untuk setiap karakter yang ada pada file yang akan di-backup.
* ```cat``` digunakan untuk membaca isi file /var/log/syslog, yang akan dienkripsi sebagai backup.
* ```tr``` digunakan untuk melakukan enkripsi pada isi file, masing-masing untuk mengubah huruf kecil dan huruf besar pada file. Pada tr, argumen pertama adalah string alfabet yang akan diubah, diikuti dengan substring yang merepresentasikan penggeseran enkripsi pada string tersebut, dan argumen kedua adalah string hasil enkripsi yang sesuai. Misalkan sekarang jam 20, oleh karena itu pada tr akan dilakukan pergantian lowercase dan uppercase alphabet dengan urutan 0:26 (urutan abcd biasa), diganti dengan lowercase dan uppercase alphabet dengan urutan {currenthour}:26 (kalau sekarang jam 20 jadinya urutan 20:26), sehingga urutan "abcdefghijklmnopqrstuvwxyz" menjadi "uvwxyzabcdefghijklmnopqrst". Lalu tr akan mengganti atau mentranslate urutan abcd biasa sesuai dengan urutan abcd yang baru.Hasil enkripsi kemudian disimpan ke dalam file backup yang telah ditentukan sebelumnya melalui variabel backup_filename.

**Untuk membuat file log_decrypt.sh**
```
$ nano log_decrypt.sh
```
Pada skrip log_decrypt, file yang sudah dienkripsi sebelumnya yang ada di direktori /home/sabrina/sisop/encrypted akan diambil, kemudian akan didekripsi kembali, kemudian akan disimpan dengan format nama sama di direktori /home/sabrina/sisop/decrypted. Sehingga, isi file yang sudah didekripsi tersebut seharusnya sama dengan isi file /var/log/syslog yang asli sebelum dienkripsi. 

Source code:
```sh
#!/bin/bash

encrypted_filename=$(ls -t /home/sabrina/soal4/encrypted | head -n1)
encrypted_filepath="/home/sabrina/soal4/encrypted/${encrypted_filename}"
decrypted_filepath="/home/sabrina/soal4/decrypted/${encrypted_filename}"

current_hour=$(date "+%H")

lowercase_alphabet=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')
uppercase_alphabet=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')

if [ -f "$encrypted_filepath" ]; then
  cat "${encrypted_filepath}" | tr "${lowercase_alphabet:${current_hour}:26}${uppercase_alphabet:${current_hour}:26}" "${lowercase_alphabet:0:26}${uppercase_alphabet:0:26}" > "${decrypted_filepath}"
fi
```

Penjelasan
* Baris kedua hingga kelima digunakan untuk mengambil file terbaru yang ada di dalam direktori encrypted. ```ls -t``` digunakan untuk menampilkan daftar file secara terurut berdasarkan waktu modifikasi terbaru, ```head -n1``` digunakan untuk mengambil baris pertama (file terbaru) dari hasil output perintah ls -t. Variabel encrypted_filename digunakan untuk menyimpan nama file terbaru tersebut, sedangkan encrypted_filepath dan decrypted_filepath digunakan untuk menyimpan path direktori encrypted dan decrypted.
* Baris ke-8 digunakan untuk menyimpan jam saat ini pada variabel current_hour.
* Baris ke-10 dan ke-11 digunakan untuk membuat variabel yang berisi dua set alfabet, yakni huruf kecil dan huruf besar. Setiap alfabet diulang sebanyak dua kali untuk menghindari kesalahan saat melakukan shift karakter.
* If digunakan untuk pengecekan apakah ada file baru di dalam direktori encrypted. ```-f``` digunakan untuk memeriksa apakah sebuah file ada atau tidak. Jika ada, maka perintah if dijalankan.
* File akan di-decrypt dengan mengubah setiap karakter sesuai dengan aturan shift cipher. ```tr``` digunakan untuk melakukan substitusi karakter. Setiap karakter pada file akan diganti dengan karakter yang terletak 26 karakter setelahnya dalam alfabet. Karena alfabet yang digunakan berulang dua kali, maka setelah mencapai huruf z atau Z, karakter akan diubah ke huruf a atau A. Hasil decrypt akan disimpan pada file baru di direktori decrypted.

Lalu untuk membuat skrip untuk melakukan backup file syslog setiap 2 jam (menjalankan log_encrypt.sh dan log_decrypt.sh secara otomatis), kita dapat menggunakan perintah cron job yaitu: 

```
$ crontab -e
```
Yang berisi skrip:
```sh
0 */2 * * * /bin/bash /home/sabrina/soal4/log_encrypt.sh
0 */2 * * * /bin/bash /home/sabrina/soal4/log_decrypt.sh
```
Screenshot crontab:<br/>
![Screenshot Crontab](https://gitlab.com/wansabrina/test-readme/-/raw/main/logfile/crontab.jpeg)

Yang dimana ```0``` menunjukkan menit ke-0, dan ```*/2``` menunjukkan 2 jam atau kode akan dijalankan tiap 2 jam sekali.

Bisa hanya menggunakan baris pertama kalau misalnya hanya ingin mengenkripsi secara otomatis, namun dengan menggunakan baris kedua, dapat dilakukan deskripsi terhadap file yang telah dienkripsi sebelumnya, namun tidak dapat dilakukan secara bersamaan untuk memunculkan file encrypt dan decrypt untuk jam yang sama, seperti sistem antri, decrypt file jam A akan dilakukan jika sudah muncul file baru, misal file encrypt jam B sudah muncul, maka decrypt untuk file encrypt jam A baru bisa dijalankan.

Contohnya, saya mengatur crontab dari jam 19.00, sehingga 2 jam kemudian file encrypt 21:00 muncul, lalu pada jam 23:00 file encrypt kedua untuk jam 23:00 muncul:
![Contoh Encyrpt](https://gitlab.com/wansabrina/test-readme/-/raw/main/logfile/contoh_encrypt.jpeg)

Namun, pada jam 23:00, file decrypt yang baru muncul hanyalah file untuk jam 21:00 tadi saja. Karena baru dijalankan saat file 23:00 sudah muncul. Dan file decrypt untuk jam 23:00 akan muncul 2 jam lagi yaitu pada jam 01:00 bersamaan dengan file encrypt jam 01:00 muncul.
![Contoh Decyrpt](https://gitlab.com/wansabrina/test-readme/-/raw/main/logfile/contoh_decrypt.jpeg)

Berikut merupakan hasil encryptnya:
![Hasil Encyrpt](https://gitlab.com/wansabrina/test-readme/-/raw/main/logfile/hasil_encrypt.jpeg)

Berikut merupakan hasil decryptnya:
![Hasil Decrypt](https://gitlab.com/wansabrina/test-readme/-/raw/main/logfile/hasil_decrypt.jpeg)

Yang dimana hasil decrypt isinya akan sama dengan isi file syslog yang asli (sebelum diencrypt), yang bisa kita lihat dengan memasuki direktori /var/log, dan melihat isi file syslog dengan perintah ```cat syslog```
![Isi file syslog asli](https://gitlab.com/wansabrina/test-readme/-/raw/main/logfile/isi_syslog_asli.jpeg)

Berikut jika program nomor 4 dijalankan secara manual dengan menggunakan ```./namafile``` untuk menjalankan/mengeksekusi file log_encrypt.sh dan log_decrypt.sh.

Log_encrypt.sh:
![Encrypt Manual](https://gitlab.com/wansabrina/test-readme/-/raw/main/logfile/encrypt_manual.jpeg)

Log_decrypt.sh
![Decrypt Manual](https://gitlab.com/wansabrina/test-readme/-/raw/main/logfile/decrypt_manual.jpeg)

**Kendala dalam pengerjaan:** Sejauh ini belum ada kendala didalam mengerjakan soal no 4.
