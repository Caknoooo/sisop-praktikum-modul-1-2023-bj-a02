#!/bin/bash

function main() {
  echo -e "Apakah anda ingin Login(1) atau Register(2)?"
  read pick

  if [ ${pick} -eq 1 ]; then
    login
  elif [ ${pick} -eq 2 ]; then
    register 
  fi
}

function loginSuccess(){
  echo -e "\nWelcome $username!!"
  echo -e "$tanggal LOGIN:INFO User $username logged in" >> log.txt
}

function loginFailed(){
  echo -e "$tanggal Login:ERROR failed login attemp on user $username" >> log.txt
  echo -e "Apakah anda sudah memiliki akun? Sudah(1) atau Belum(2)"
  read pick
  if [[ $pick -eq 1 ]]; then
    login
  elif [[ $pick -eq 2 ]]; then 
    register
  fi
}

function login(){
  echo -e "\n----------------------------------------------------------------------"
  echo -e "Selamat Datang di Aplikasi Kami, silahkan login dengan Username anda!"
  echo -e "Enter Username: \c"
  read username

  echo -e "Enter Password: \c"
  read -s password
  echo -e "\n----------------------------------------------------------------------"

  tanggal=$(date +'%m/%d/%Y %H:%M:%S')
  tanggal_folder=$(date +'%Y-%m-%d')_$username
  # echo $tanggal
  # echo $tanggal_folder
  # loginFailed

  if grep -qi $username ./users/user.txt && grep -qi $password ./users/user.txt; then
    loginSuccess
  else
    loginFailed
  fi
}

function register(){
  ./louis.sh
}

main