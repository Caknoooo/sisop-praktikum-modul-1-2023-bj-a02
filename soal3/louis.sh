#!/bin/bash

function check_user() {
  if grep -qi ${username} ./users/user.txt; then
    echo "Username sudah terdaftar, silahkan masukkan username yang lain"
    echo "${tanggal} REGISTER:ERROR user already exist" >> log.txt
  elif echo ${username} | grep -Eq "chicken|ernie"; then
    echo "Username tidak boleh mengandung 'chicken' atau 'ernie'"
  else
    return 0
  fi
  return 1
}

function check_password() {
  if [[ ${#password} -lt 8 ]]; then
    echo "Password harus memiliki minimal 8 karakter!"
  else
    # echo ${password}
    # echo "$password"
    if [[ ! ${password} =~ [A-Z] ]]; then
      echo "Password harus mengandung 1 huruf kapital"
    elif [[ ! ${password} =~ [a-z] ]]; then
      echo "Password harus mengandung 1 huruf kecil"
    elif [[ ! ${password} =~ [0-9] ]]; then
      echo "Password harus mengandung 1 alphanumeric"
    elif [[ ! ${password} =~ [[:upper:]] || ! ${password} =~ [[:lower:]] ]]; then
      echo "Password harus mengandung minimal 1 huruf kapital dan 1 huruf kecil"
    elif [[ ${username} == ${password} ]]; then
      echo "Nama dan password tidak boleh sama"
    else
      echo "Registrasi berhasil 😀😀😀"
      echo "selamat datang $username"
      echo -e "Username : ${username} - Password : ${password}" >>./users/user.txt
      echo -e "${tanggal} Register:INFO User ${username} registered successfully" >>log.txt
    fi
  fi
}

#  Main function
function main() {

  tanggal=$(date '+%m/%d/%Y %H:%M:%S')
  # echo $tanggal
  echo -e "\n----------------------------------------------------------------------"
  echo -e "Selamat Datang di Aplikasi Kami, Mohon Registrasi Terlebih Dahulu 😃"
  echo -e ""

  echo -e "Enter Username: \c"
  read username

  echo -e "Enter Password: \c"
  read -s password
  echo -e "\n----------------------------------------------------------------------"
  echo ""
}

main

if check_user; then
  check_password
fi
