#!/bin/bash

echo ""
# No 1A
echo "Menampilkan 5 universitas tertinggi di negara jepang"
awk -F"," '/Japan/ {print $2, $9}' '2023 QS World University Rankings.csv' | head -5 | sort -t',' -k1 -nr 
echo ""

# No 1B
echo "Menampilkan nilai terendah dari 5 universitas di jepang"
awk '/Japan/' "2023 QS World University Rankings.csv" | sort -t',' -k9,9n | sed -n 1,5p | cut -d',' -f2
echo ""

# No 1C
echo "Menampilkan 10 universitas ger rank tertinggi"
awk '/Japan/' "2023 QS World University Rankings.csv" | sort -t',' -n -k20,20 | sed -n 1,10p | cut -d',' -f2
echo ""

# No 1D
echo "Menampilkan Universitas paling keren :)"
awk '/Keren/' "2023 QS World University Rankings.csv" | cut -d',' -f2
echo"" 
