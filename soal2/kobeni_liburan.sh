#!/bin/bash
2a(){
for ((i=1; ; i++))
do
  if [ ! -d kumpulan_$i ]; then
    break
  fi
done

# Buat folder baru
mkdir kumpulan_$i

# Masuk ke folder baru
cd kumpulan_$i

# Looping sesuai waktu
if [ "$(date +"%H")" == "00" ]; then
  # Download satu gambar
  wget https://source.unsplash.com/random/800x600/?indonesia -O perjalanan_1.jpg
else
  for ((j=1; j<=$(date +"%H"); j++))
  do
    # Download gambar
    wget https://source.unsplash.com/random/800x600/?indonesia -O perjalanan_$j.jpg
  done
fi

# Crontab
# 0 */10 * * * /bin/bash /home/vangarman/Documents/Praktikum\ Sisop/sisop-praktikum-modul-1-2023-bj-a02/soal2/kobeni_liburan.sh 2a
}

2b(){
for ((k=1; ;k++)) do
if [ ! -d kumpulan_$k ]
  then
     break
  fi
  
  if [ -f devil_$k.zip ]
  then
    continue
  fi
  
  # Zip folder kumpulan
    zip -r devil_$k.zip kumpulan_$k
 
done

# Crontab
# @daily /bin/bash /home/vangarman/Documents/Praktikum\ Sisop/sisop-praktikum-modul-1-2023-bj-a02/soal2/kobeni_liburan.sh 2b
}

# Pemanggilan fungsi sesuai argumen yang diberikan
if [ "$1" = "2a" ]; then
  2a
elif [ "$1" = "2b" ]; then
  2b
fi
