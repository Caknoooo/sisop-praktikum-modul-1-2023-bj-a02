#!/bin/bash

encrypted_filename=$(ls -t /home/sabrina/soal4/encrypted | head -n1)
encrypted_filepath="/home/sabrina/soal4/encrypted/${encrypted_filename}"
decrypted_filepath="/home/sabrina/soal4/decrypted/${encrypted_filename}"

current_hour=$(date "+%H")

lowercase_alphabet=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')
uppercase_alphabet=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')

if [ -f "$encrypted_filepath" ]; then
  cat "${encrypted_filepath}" | tr "${lowercase_alphabet:${current_hour}:26}${uppercase_alphabet:${current_hour}:26}" "${lowercase_alphabet:0:26}${uppercase_alphabet:0:26}" > "${decrypted_filepath}"
fi

## crontab untuk menjalankan log_decrypt secara otomatis setiap 2 jam sekali
# 0 */2 * * * /bin/bash /home/sabrina/soal4/log_decrypt.sh
