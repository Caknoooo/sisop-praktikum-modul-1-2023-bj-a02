#!/bin/bash

current_hour=$(date "+%H")
current_date=$(date "+%H:%M %d:%m:%Y")

backup_filename="/home/sabrina/soal4/encrypted/${current_date}.txt"

lowercase_alphabet=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')
uppercase_alphabet=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')

cat /var/log/syslog | tr "${lowercase_alphabet:0:26}${uppercase_alphabet:0:26}" "${lowercase_alphabet:${current_hour}:26}${uppercase_alphabet:${current_hour}:26}" > "${backup_filename}"

## crontab untuk menjalankan log_encrypt secara otomatis setiap 2 jam sekali
# 0 */2 * * * /bin/bash /home/sabrina/soal4/log_encrypt.sh
